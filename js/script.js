let tabButtons = document.querySelectorAll('.tabs-title'),
    tabs = document.querySelectorAll('.tabs-content li'),
    //класс вынес для удобства
    activeClass = 'active'


    //start
bindTabListener(tabButtons,tabs,'button','tab')



//bind Listener to buttons
function bindTabListener(elements, tabs, datasetButton, datasetTab) {

    // console.log(elements.length,tabs)
    if(elements.length > 0 && tabs.length > 0)
    {
        elements = [...elements]
        tabs = [...tabs]

        elements.map(element => element.addEventListener('click', function() {
            
            //change active button
            changeActiveButton(this)

            //display only equal dataset
            tabs.map(tab => {
                if(tab.dataset[datasetTab] === this.dataset[datasetButton])
                {
                    tab.style.display = "block"
                }
                else
                {
                    tab.style.display = "none"
                }
            })
        }))
    }
}

// change .active element from old to new
function changeActiveButton(newActiveElem) {
    let oldActive = document.querySelector(`.${activeClass}`)
    if(oldActive)
    {
        oldActive.classList.remove(`${activeClass}`)
        newActiveElem.classList.add(`${activeClass}`)
    }
}